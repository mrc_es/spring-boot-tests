@RestController
class ThisWillActuallyRun {

    @RequestMapping("/")
    String home() {
        "Hello World!"
    }

    @RequestMapping("/route1")
    static String route1() {
        "New route!"
    }

}