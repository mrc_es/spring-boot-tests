package com.cuaukafkaspring.cuaukafkaspring;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.*;

@RestController
@SpringBootApplication
public class CuauKafkaSpringApplication {

	public static void main(String[] args) {
		SpringApplication.run(CuauKafkaSpringApplication.class, args);
	}

	@RequestMapping("/")
	String home() {
		return "Hello World!";
	}
}

